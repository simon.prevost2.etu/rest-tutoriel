package fr.ulille.iut.tva.dto;

import fr.ulille.iut.tva.service.CalculTva;
import fr.ulille.iut.tva.service.TauxTva;

public class InfoDetails {
	private String tauxLabel;
    private double tauxValue;
    private double montantTotal;
    private double montantTva;
    private double somme;
    
    private CalculTva calculTva = new CalculTva();
    
    public InfoDetails() {}
    
    public InfoDetails(String label, double somme) {
        this.tauxLabel = label.toUpperCase();
        this.tauxValue = TauxTva.valueOf(label.toUpperCase()).taux;
        this.somme = somme;
        this.montantTotal = calculTva.calculerMontant(TauxTva.valueOf(label.toUpperCase()),somme);;
        this.montantTva = this.montantTotal-somme;
    }

	public String getTauxLabel() {
		return tauxLabel;
	}

	public void setTauxLabel(String tauxLabel) {
		this.tauxLabel = tauxLabel;
	}

	public double getTauxValue() {
		return tauxValue;
	}

	public void setTauxValue(double tauxValue) {
		this.tauxValue = tauxValue;
	}

	public double getMontantTotal() {
		return montantTotal;
	}

	public void setMontantTotal(double montantTotal) {
		this.montantTotal = montantTotal;
	}

	public double getMontantTva() {
		return montantTva;
	}

	public void setMontantTva(double montantTva) {
		this.montantTva = montantTva;
	}

	public double getSomme() {
		return somme;
	}

	public void setSomme(double somme) {
		this.somme = somme;
	}

	public CalculTva getCalculTva() {
		return calculTva;
	}

	public void setCalculTva(CalculTva calculTva) {
		this.calculTva = calculTva;
	}
    
}
